import numpy as np
from copy import deepcopy

# logistic probability 
def logistic_prob(hrs,betas):
    return 1 / ( 1 + np.exp(-(betas[0] + betas[1]*hrs)) )

# objective functions
global_counter = 0
def total_err_and_grad(dat,betas):
    global global_counter
    global_counter += 1
    tot = 0
    tot_grad = np.array([0.,0.])
    (nhrs,passfail) = dat
    for i in range(len(nhrs)):
        prediction = logistic_prob(nhrs[i],betas)
        difference = passfail[i] - prediction
        tot      += difference*difference
        tot_grad -= difference*prediction*(1-prediction)*np.array([1.,nhrs[i]])
    return tot, tot_grad

# data input
def read_data(filename):
    f = open(filename,'r')
    hrs = f.readline().split()
    passing = f.readline().split()
    hrs.pop(0)
    passing.pop(0)
    return ([float(h) for h in hrs],
             [ int(p) for p in passing])


# To evaluate phi and derivative of phi (for line search below)
def dotProductWithGradient(dat,betas,direction,evalObjGrad):
    fval,fgrad =  evalObjGrad(dat,betas);
    return fval, np.dot(direction,fgrad)
    
# this is part of line search algorithm (Nocedal+Wright;2nd Edt.; p.61)
def zoom(dat,betas,c1,c2,w0,direction,phiV0,phiD0,alphaLo,phiVLo,phiDLo,alphaHi,phiVHi,evalObjGrad):
    while True:
        # Interpolate for alpha (quadratic interpolation)
        alpha = -phiDLo / 2 * (alphaHi-alphaLo)**2 / (phiVHi - phiVLo -phiDLo*(alphaHi-alphaLo))
        alpha = alpha + alphaLo
        # Evaluate phi(xk + alpha)
        phiVj, phiDj = dotProductWithGradient(dat,betas+alpha*direction,direction,evalObjGrad)
        # Check if the first Wolfe condition is satisfied
        if phiVj > phiV0 + c1*alpha * phiD0 or phiVj >= phiVLo :
            alphaHi = alpha
            phiVHi  = phiVj
        else:
            if np.fabs(phiDj) < - c2*phiD0:
                break
            if phiDj*(alphaHi-alphaLo) >= 0:
                alphaHi = alphaLo
                phiVHi  = phiVLo
            alphaLo = alpha
            phiVLo = phiVj
            phiDLo = phiDj
    return alpha
        
# line search (Nocedal+Wright;2nd Edt.; p.60)
def lineSearch(dat,betas,alpha,direction,evalObjGrad):
    alpha0 = 0
    c1 = 0.0001
    c2 = 0.1
    phiV0, phiD0 = dotProductWithGradient(dat,betas,direction,evalObjGrad)
    phiVi_old = phiV0
    phiDi_old = phiD0
    alpha_old = alpha0

    w0 = deepcopy( betas )
    
    i = 1
    while True:
        # evaluate phi(alpha)
        phiVi, phiDi = dotProductWithGradient(dat,betas+alpha*direction,direction,evalObjGrad)
        if phiVi > phiV0 + c1*alpha * phiD0 or ( phiVi >= phiVi_old and i>1 ):
            alpha = zoom(dat,betas,c1,c2,w0,direction,phiV0,phiD0,alpha_old,phiVi_old,phiDi_old,alpha,phiVi,evalObjGrad)
            break
        if np.fabs(phiDi) < - c2*phiD0:
            break
        if phiDi >= 0:
            alpha = zoom(dat,betas,c1,c2,w0,direction,phiV0,phiD0,alpha,phiVi,phiDi,alpha_old,phiVi_old,evalObjGrad)
            break
        phiVi_old = phiVi
        phiDi_old = phiDi
        alpha_old = alpha
        alpha *= 2
        i += 1
    return alpha

#------------------------------------------------------------------
dat1 = read_data("file1.txt")

thetas = np.array([0.,0.])
alpha = 1

# Fletcher-Reeves (Nocedal+Wright;2nd Edt.; p.121)
k = 0
fval_k, fgrad_k = total_err_and_grad(dat1,thetas)
p_k = -fgrad_k
while np.dot(fgrad_k,fgrad_k) > 1e-10*(1+np.abs(fval_k))*(1+np.abs(fval_k)):
    alpha = lineSearch(dat1,thetas,alpha,p_k,total_err_and_grad)
    thetas = thetas + alpha*p_k
    fval_k_p1, fgrad_k_p1 = total_err_and_grad(dat1,thetas)
    beta_k_p1 = np.dot(fgrad_k_p1,fgrad_k_p1)/np.dot(fgrad_k,fgrad_k)
    k += 1
    if (k%thetas.size==0): # reset
        beta_k_p1 = 0
    p_k = -fgrad_k_p1 + beta_k_p1 * p_k
    fval_k = fval_k_p1
    fgrad_k = deepcopy( fgrad_k_p1 )
    print( k, thetas )
print( "total number of function evaluations: ", global_counter )
    
